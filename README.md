Project features:
1. A client-server application built with no use of any framework
2. Server replies JSON format, with data :
3. 'uptime' - returns server runtime
4. 'info' - returns server`s version, creation time
5. 'help' - returns a list of mentioned commands with descriptions
6. 'stop' - stops running both server and client

After app's done, make a short video of it running
Comment about problems
