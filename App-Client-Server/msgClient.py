import sys
import selectors
import json
import io
import struct

# typical web message structure : 2 bytes fixed length integer message on how long is next JSON message, then
# a JSON variable length header with info about content message : type, encoding, and length
# third part is message of type, encoding, length specified in JSON header


class Message:
    def __init__(self, selector, sock, addr, request):
        self.selector = selector
        self.sock = sock
        self.addr = addr
        self.request = request
        self._recv_buffer = b""
        self._send_buffer = b""
        self._request_queued = False
        self._jsonheader_len = None
        self.jsonheader = None
        self.response = None

    def _set_selector_events_mask(self, mode):
        # Set selector to listen for events: mode is 'r', 'w', or 'rw'
        if mode == "r":
            events = selectors.EVENT_READ
        elif mode == "w":
            events = selectors.EVENT_WRITE
        elif mode == "rw":
            events = selectors.EVENT_READ | selectors.EVENT_WRITE
        else:
            raise ValueError(f"Invalid events mask mode {mode!r}.")
        self.selector.modify(self.sock, events, data=self)

    def _read(self):
        try:
            # should be read to read
            data = self.sock.recv(4096)
        except BlockingIOError:
            # resource temporarily unavailable (errno EWOULDBLOCK)
            pass
        else:
            if data:
                self._recv_buffer += data
            else:
                raise RuntimeError("Peer closed")

    def _write(self):
        if self._send_buffer:
            print(f"Sending {self._send_buffer!r} to {self.addr}")
            try:
                # should be ready to write
                sent = self.sock.send(self._send_buffer)
            except BlockingIOError:
                # resource temporarily unavailable (errno EWOULDBLOCK)
                # this is important for catching temporary error and skipping it with pass, so a new call is made
                pass
            else:
                self._send_buffer = self._send_buffer[sent:]

    def _json_encode(self, obj, encoding):
        return json.dumps(obj, ensure_ascii=False).encode(encoding)

    def _json_decode(self, json_bytes, encoding):
        tiow = io.TextIOWrapper(
            io.BytesIO(json_bytes), encoding=encoding, newline=""
        )
        obj = json.load(tiow)
        tiow.close()
        return obj

    def _create_message(self, *, content_bytes, content_type, content_encoding):
        jsonheader = {
            "byteorder": sys.byteorder,
            "content-type": content_type,
            "content-encoding": content_encoding,
            "content-length": len(content_bytes),
        }
        jsonheader_bytes = self._json_encode(jsonheader, "utf-8")
        # struct module is used to pack or unpack binary data using format strings
        message_hdr = struct.pack(">H", len(jsonheader_bytes))
        message = message_hdr + jsonheader_bytes + content_bytes
        return message

    def _process_response_json_content(self):
        content = self.response
        result = content.get("result")
        print(f"Got result: {result}")

    def _process_response_binary_content(self):
        content = self.response
        print(f"Got response: {content!r}")

    # process_events will be called many times during connection
    def process_events(self, mask):
        if mask & selectors.EVENT_READ:
            self.read()
        if mask & selectors.EVENT_WRITE:
            self.write()

    def read(self):
        self._read()

        if self._jsonheader_len is None:
            self.process_protoheader()
        if self._jsonheader_len is not None:
            if self.jsonheader is None:
                self.process_jsonheader()
        if self.jsonheader:
            if self.request is None:
                self.process_response()

    def write(self):
        # the client initiates connection, thus sends request first to queue a request
        if not self._request_queued:
            # this method creates request and send it to send buffer, and changes _request_queued flag, to call it once
            self.queue_request()
        # _write calls socket.send if there is data in the socket
        self._write()
        if self._request_queued:
            # last is to check if request is queued, so if not to change it to listen to read events
            if not self._send_buffer:
                # set selector to listen for read events, we are done writing
                self._set_selector_events_mask("r")

    def close(self):
        print(f"Closing connection to {self.addr}")
        try:
            self.selector.unregister(self.sock)
        except Exception as e:
            print(
                f"Error: selector.unregister() exception for "
                f"{self.addr}: {e!r}"
            )
        try:
            self.sock.close()
        except OSError as e:
            print(f"Error: socket.close() exception for {self.addr}: {e!r}")
        finally:
            # delete reference to socket object for garbage collection
            self.sock = None

    # it`s the first task for the client to queue the request, dictionaries used to create request are in appClient
    # request dictionary is passed as argument to the class when Message object is created
    def queue_request(self):
        content = self.request["content"]
        content_type = self.request["type"]
        content_encoding = self.request["encoding"]
        if content_type == "text/json":
            req = {
                "content_bytes": self._json_encode(content, content_encoding),
                "content_type": content_type,
                "content_encoding": content_encoding,
            }
        else:
            req = {
                "content_bytes": content,
                "content_type": content_type,
                "content_encoding": content_encoding,
            }
        # message is created and appended to _send_buffer, it`s then seen and sent by _write()
        message = self._create_message(**req)
        self._send_buffer += message
        # state variable is then changed so that .queue_request() isn`t called again
        self._request_queued = True

    # protoheader is initial 2 bytes in network byte order header, with data on length of JSON header
    def process_protoheader(self):
        hdrlen = 2
        # instruction to unpack data form _recv_buffer and pass value of JSON len to _jsonheader_len, then clean data
        if len(self._recv_buffer) >= hdrlen:
            self._jsonheader_len = struct.unpack(">H", self._recv_buffer[:hdrlen])[0]
            self._recv_buffer = self._recv_buffer[hdrlen:]

    # JSON header length is passed from protoheader and decoded
    def process_jsonheader(self):
        hdrlen = self._jsonheader_len
        if len(self._recv_buffer) >= hdrlen:
            # decoding all data up to hdrlen value to JSON and write to variable
            self.jsonheader = self._json_decode(self._recv_buffer[:hdrlen], "utf-8")
            # clean _recv_buffer up to hdrlen value
            self._recv_buffer = self._recv_buffer[hdrlen:]
            # checking if standard JSON header keys are present
            for reqhdr in (
                "byteorder",
                "content-type",
                "content-encoding",
                "content-length",
            ):
                if reqhdr not in self.jsonheader:
                    raise ValueError(f"Missing required header '{reqhdr}'.")

    # it`s just barely different from Server .create_response() method
    def process_response(self):
        content_len = self.jsonheader["content-length"]
        if not len(self._recv_buffer) >= content_len:
            return
        data = self._recv_buffer[:content_len]
        self._recv_buffer = self._recv_buffer[content_len:]
        if self.jsonheader["content-type"] == "text/json":
            encoding = self.jsonheader["content-encoding"]
            self.response = self._json_decode(data, encoding)
            print(f"Received response {self.response!r} from {self.addr}")
            self._process_response_json_content()
        else:
            # binary or unknown content type
            self.response = data
            print(
                f"Received {self.jsonheader['content-type']}"
                f"response from {self.addr}"
            )
            self._process_response_binary_content()
        # close when response has been processed
        self.close()
