import sys
import socket
import selectors
import traceback

import msgServer

sel = selectors.DefaultSelector()


# an instance of a class for each socket in the client and server when connection is created or accepted
def accept_wrapper(sock):
    conn, addr = sock.accept()
    print(f"Accept connection from {addr}")
    conn.setblocking(False)
    # after a message object is created it`s associated with a socket that`s monitored for events
    # initially set to read events, once request has been read, modified to listen to write only
    message = msgServer.Message(sel, conn, addr)
    # selector.register() allows monitoring socket for events
    # if you told sel. to also monitor for WRITE, the event loop will call .send() too early, wasting CPU power
    sel.register(conn, selectors.EVENT_READ, data=message)


if len(sys.argv) != 3:
    print(f"Usage: {sys.argv[0]} <host> <port>")
    sys.exit(1)


# hostname usage - for loopback interface use 127.0.0.1 or ::1, for ethernet interface an IP address or empty string,
# if you want to use more than one interface

host, port = sys.argv[1], int(sys.argv[2])
lsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# avoid bind(), because Exception OSError [error number 48], address already in use
# after creating socket call is made to setsockopt with socket.SO_REUSEADDR - to avoid 'address already in use error'
# this will be visible with servet ona  port with TIME_WAIT state - even for 2 minutes OSError may be present
# TIME_WAIT is to ensure any delayed packet may have a chance to get to right server/port/host, not wrong application
lsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
lsock.bind((host, port))
lsock.listen()
print(f"Listening on {(host, port)}")
# socket function that temporarily suspends app is blocking call - .accept(), .connect(), .send(), .recv(), etc
# blocking calls have to wait for system calls I/O to complete before returning a value
# setting to non-blocking allows returning value without waiting, but data may not be ready
lsock.setblocking(False)
sel.register(lsock, selectors.EVENT_READ, data=None)

try:
    while True:
        # when events are ready on the socket they are returned by sel - selector.select(), it`s leading event loop
        # blocking, waiting at top of loop for events. Wakes up when read and write events are ready to be processed
        # on the socket. Also, responsible for calling .process_events() from msg-server class.
        events = sel.select(timeout=None)
        for key, mask in events:
            if key.data is None:
                accept_wrapper(key.fileobj)
            else:
                # you can get reference back to the message object using data attribute on key object,
                # and call a method in Message
                message = key.data
                try:
                    message.process_events(mask)
                except Exception:
                    print(
                        f"Main: Error: Exception for {message.addr}:\n"
                        f"{traceback.format_exc()}"
                    )
                    message.close()
except KeyboardInterrupt:
    print("Caught keyboard interrupt, exiting")
finally:
    # it`s possible to have half-open connection either by server or client side
    sel.close()
