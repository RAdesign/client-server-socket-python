# to handle various exceptions including different data-types, headers will be added
# typical header : variable-length text, unicode with UTF-8 encoding, python dictionary serialized using JSON
# sub-headers like : 1.byteorder - byte order the machine uses (sys.byteorder). may not be required here.
# 2.content-length - length of content in bytes
# 3.content-type - type of content in payload, f.ex. text/json or binary/my-binary-type
# 4.content-encoding - encoding used by content, f.ex. utf-8 for unicode or binary for binary data
# problem with header length with .recv() - use small fixed len. header to prefix the JSON with real len. and info

import sys
import socket
import selectors
import traceback

import msgClient

sel = selectors.DefaultSelector()


def create_request(action, value):
    # client sends a search request, like this, with connection, to server to match it
    if action == "search":
        return dict(
            type="text/json",
            encoding="utf-8",
            content=dict(action=action, value=value),
        )
    else:
        # if search request is not recognized by server it`s assumed it`s a binary response
        return dict(
            type="binary/custom-client-binary-type",
            encoding="binary",
            content=bytes(action + value, encoding="utf-8"),
        )


def start_connection(host, port, request):
    addr = (host, port)
    print(f"Starting connection to{addr}")
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.setblocking(False)
    sock.connect_ex(addr)
    events = selectors.EVENT_READ | selectors.EVENT_WRITE
    # an instance of a class for each socket in the client and server when connection is created or accepted
    message = msgClient.Message(sel, sock, addr, request)
    # like for the Server Message object is associated with socket object, with sel.register
    # but for Client socket monitors for both events of read and write initially, but changed to listen for read
    # after request is written - it`s too for saving CPU power
    sel.register(sock, events, data=message)

# hostname usage - for loopback interface use 127.0.0.1 or ::1, for ethernet interface an IP address or empty string,
# if you want to use more than one interface
# if appClient is called with wrong number of parameters, help message is printed:
if len(sys.argv) != 5:
    print(f"Usage {sys.argv[0]} <host> <port> <action> <value>")
    sys.exit(1)

host, port = sys.argv[1], int(sys.argv[2])
action, value = sys.argv[3], sys.argv[4]
# a request dictionary used to create sockets for each connection and Message objects
request = create_request(action, value)
# initial appClient parameters are passed to .start_connection()
start_connection(host, port, request)

try:
    while True:
        events = sel.select(timeout=1)
        for key, mask in events:
            message = key.data
            try:
                message.process_events(mask)
            # any exceptions raised by the class are caught by the main script in the event loop
            except Exception:
                print(
                    f"Main: Error: Exception for {message.addr}:\n"
                    f"{traceback.format_exc()}"
                )
                # message.close() is important for closing socket, and removing socket from being monitored by .select()
                message.close()
        # check for a socket being monitored to continue
        if not sel.get_map():
            break
except KeyboardInterrupt:
    print("Caught keyboard interrupt, exiting")
finally:
    sel.close()
