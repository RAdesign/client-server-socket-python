import sys
import selectors
import json
import io
import struct

request_search = {
    "morfeusz": "czerwona czy niebieska. \U0001f430",
    "pierscien": "w jaskiniach pod mglista gora. \U0001f48d",
    "\U0001f436": "\U0001f43e gramy ! \U0001f3d0",
}


class Message:
    def __init__(self, selector, sock, addr):
        self.selector = selector
        self.sock = sock
        self.addr = addr
        self._recv_buffer = b""
        self._send_buffer = b""
        self._jsonheader_len = None
        self.jsonheader = None
        self.request = None
        self.response_created = False

    def _set_selector_events_mask(self, mode):
        # Set selector to listen for events: mode is 'r', 'w', or 'rw'
        if mode == "r":
            events = selectors.EVENT_READ
        elif mode == "w":
            events = selectors.EVENT_WRITE
        elif mode == "rw":
            events = selectors.EVENT_READ | selectors.EVENT_WRITE
        else:
            raise ValueError(f"Invalid events mask mode {mode!r}.")
        self.selector.modify(self.sock, events, data=self)

    def _read(self):
        # this method is called first, calls socket.recv to read data from socket and store in receive buffer
        try:
            # should be read to read
            data = self.sock.recv(4096)
        except BlockingIOError:
            # resource temporarily unavailable (errno EWOULDBLOCK)
            pass
        else:
            # data may be arriving in parts, this checks if all data has arrived
            if data:
                self._recv_buffer += data
            else:
                raise RuntimeError("Peer closed")

    # _write method takes care for problems with buffer by checking state of message several times
    def _write(self):
        # create_response triggers this method
        if self._send_buffer:
            print(f"Sending {self._send_buffer!r} to {self.addr}")
            try:
                # should be ready to write
                sent = self.sock.send(self._send_buffer)
            except BlockingIOError:
                # this is important for catching temporary error and skipping it with pass, so a new call is made
                # resource temporarily unavailable (errno EWOULDBLOCK)
                pass
            else:
                self._send_buffer = self._send_buffer[sent:]
                # close when the buffer is drained. The response has been sent.
                if sent and not self._send_buffer:
                    self.close()

    def _json_encode(self, obj, encoding):
        return json.dumps(obj, ensure_ascii=False).encode(encoding)

    def _json_decode(self, json_bytes, encoding):
        tiow = io.TextIOWrapper(
            io.BytesIO(json_bytes), encoding=encoding, newline=""
        )
        obj = json.load(tiow)
        tiow.close()
        return obj

    def _create_message(self, *, content_bytes, content_type, content_encoding):
        jsonheader = {
            "byteorder": sys.byteorder,
            "content-type": content_type,
            "content-encoding": content_encoding,
            "content-length": len(content_bytes),
        }
        jsonheader_bytes = self._json_encode(jsonheader, "utf-8")
        message_hdr = struct.pack(">H", len(jsonheader_bytes))
        message = message_hdr + jsonheader_bytes + content_bytes
        return message

    def _create_response_json_content(self):
        action = self.request.get("action")
        if action == "search":
            query = self.request.get("value")
            answer = request_search.get(query) or f"No match for '{query}'."
            content = {"result": answer}
        else:
            content = {"result": f"Error: invalid action '{action}'."}
        content_encoding = "utf-8"
        response = {
            "content_bytes": self._json_encode(content, content_encoding),
            "content_type": "text/json",
            "content_encoding": content_encoding,
        }
        return response

    def _create_response_binary_content(self):
        response = {
            "content_bytes": b"First 10 bytes of request: " + self.request[:10],
            "content_type": "binary/custom-server-binary-type",
            "content_encoding": "binary",
        }
        return response

    # manages state in a simple way - keeps logic simple, as events com in on socket for processing
    # process_events will be called many times during connection
    def process_events(self, mask):
        if mask & selectors.EVENT_READ:
            self.read()
        if mask & selectors.EVENT_WRITE:
            self.write()

    def read(self):
        # this calls _read() first to get all data from recv_buffer() from socket.recv()
        self._read()
        # checking if all bytes are there, state checks at each part of message - 2bytes, JSON, content
        # fixed-len header process_protoheader for self._jsonheader_len output
        if self._jsonheader_len is None:
            self.process_protoheader()
        # JSON header process_jsonheader() for self.jsonheader output
        if self._jsonheader_len is not None:
            if self.jsonheader is None:
                self.process_jsonheader()
        # content of message , process_request() for self.request output
        if self.jsonheader:
            if self.request is None:
                self.process_request()

    def write(self):
        # write method checks for request from process_request (instance initially has None)
        if self.request:
            # if there is one but no response, creates one calling method (instance initially has False)
            if not self.response_created:
                self.create_response()
        # the _write() method calls socket.send() if there is any data in the buffer
        # many state checks in _write are for cases if not all data has been given or queued yet
        self._write()

    def close(self):
        print(f"Closing connection to {self.addr}")
        try:
            self.selector.unregister(self.sock)
        except Exception as e:
            print(
                f"Error: selector.unregister() exception for "
                f"{self.addr}: {e!r}"
            )
        try:
            self.sock.close()
        except OSError as e:
            print(f"Error: socket.close() exception for {self.addr}: {e!r}")
        finally:
            # delete reference to socket object for garbage collection
            self.sock = None

    # protoheader is initial 2 bytes in network byte order header, with data on length of JSON header
    def process_protoheader(self):
        hdrlen = 2
        # instruction to unpack data form _recv_buffer and pass value of JSON len to _jsonheader_len, then clean data
        if len(self._recv_buffer) >= hdrlen:
            # use struct to unpack read value, store it in _len
            self._jsonheader_len = struct.unpack(">H", self._recv_buffer[:hdrlen])[0]
            # after it is stored in _len, _recv_buffer cleans itself
            self._recv_buffer = self._recv_buffer[hdrlen:]

    # JSON header length is passed from protoheader and decoded
    def process_jsonheader(self):
        hdrlen = self._jsonheader_len
        if len(self._recv_buffer) >= hdrlen:
            # decoding all data up to hdrlen value, deserialize to JSON and write to header variable
            self.jsonheader = self._json_decode(self._recv_buffer[:hdrlen], "utf-8")
            # clean _recv_buffer up to hdrlen value of JSON header (but not content)
            self._recv_buffer = self._recv_buffer[hdrlen:]
            # checking if standard JSON header keys are present
            for reqhdr in (
                "byteorder",
                "content-type",
                "content-encoding",
                "content-length",
            ):
                if reqhdr not in self.jsonheader:
                    raise ValueError(f"Missing required header '{reqhdr}'.")

    def process_request(self):
        content_len = self.jsonheader["content-length"]
        if not len(self._recv_buffer) >= content_len:
            return
        # message content is saved to data variable, then content is removed from _recv_buffer
        data = self._recv_buffer[:content_len]
        self._recv_buffer = self._recv_buffer[content_len:]
        # if JSON, it`s processed
        if self.jsonheader["content-type"] == "text/json":
            encoding = self.jsonheader["content-encoding"]
            self.request = self._json_decode(data, encoding)
            print(f"Received request {self.request!r} from {self.addr}")
        else:
            # binary or unknown content type
            self.request = data
            print(
                f"Received {self.jsonheader['content-type']}"
                f"request from {self.addr}"
            )
        # set selector to listen to write events, we are done reading
        self._set_selector_events_mask("w")

    # method sets response_created state and writes response to send buffer
    # response is created by calling other methods, here for action==search dictionary lookup.
    # you can call any methods you like here
    def create_response(self):
        if self.jsonheader["content-type"] == "text/json":
            response = self._create_response_json_content()
        else:
            # binary or unknown content type
            response = self._create_response_binary_content()
        message = self._create_message(**response)
        # after creating response message, state variable is created, so .write() won`t call .create_response again
        self.response_created = True
        # finally response is appended to buffer, ant then sent via _write
        # the _write takes care for state checks, because buffer may be full etc. problems
        self._send_buffer += message
        # tricky part is when to close connection, here it is a part of _write()
        # it`s acceptable because Message class only handles one message per connection
