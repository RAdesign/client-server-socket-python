import sys
import socket
import selectors
import types

sel = selectors.DefaultSelector()
messages = [b"Message 1 from client.", b"Message 2 from client."]


# initiating connections with function
# num_conns read from CLI, means number of connections to be created to the server
def start_connections(host, port, num_conns):
    server_addr = (host, port)
    for i in range(0, num_conns):
        connid = i + 1
        print(f"Starting connection {connid} to {server_addr}")
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # just like in server each socket is set to non-blocking mode
        sock.setblocking(False)
        # simple connect() will raise IO exception, and interfere with connection in progress
        sock.connect_ex(server_addr)
        # connection complete, socket is ready and return with .select()
        events = selectors.EVENT_READ | selectors.EVENT_WRITE
        # data you want to store with socket is created using SimpleNamespace
        data = types.SimpleNamespace(
            connid=connid,
            msg_total=sum(len(m) for m in messages),
            recv_total=0,
            # messages that client will send to the server are copied, because each connection will call
            # socket.send() and modify the list
            messages=messages.copy(),
            outb=b"",
        )
        # everything about sending, sent, needs to send, received,total bytes - is in data object
        sel.register(sock, events, data=data)


# how the client connection is handled, some differences with the server.
# server depends on client, so it will close connection after done sending messages
# in real situations  - implement timeout on server connections
def service_connection(key, mask):
    sock = key.fileobj
    data = key.data
    if mask & selectors.EVENT_READ:
        recv_data = sock.recv(1024)  # should be ready to read
        if recv_data:
            # server difference, keeping track of data received from server, so to close connection in right time
            print(f"Received {recv_data!r} from connection {data.connid}")
            data.recv_total += len(recv_data)
        if not recv_data or data.recv_total == data.msg_total:
            print(f"Closing connection {data.connid}")
            sel.unregister(sock)
            sock.close()
    if mask & selectors.EVENT_WRITE:
        if not data.outb and data.messages:
            data.outb = data.messages.pop(0)
        if data.outb:
            print(f"Sending {data.outb!r} to connection {data.connid}")
            sent = sock.send(data.outb)
            data.outb = data.outb[sent:]


if len(sys.argv) != 4:
    print(f"Usage: {sys.argv[0]} <host> <port> <num_connections")
    sys.exit(1)

host, port, numm_cons = sys.argv[1:4]
start_connections(host, int(port), int(numm_cons))

try:
    while True:
        events = sel.select(timeout=1)
        if events:
            for key, mask in events:
                service_connection(key, mask)
        # check for a socket being monitored to continue
        if not sel.get_map():
            break
except KeyboardInterrupt:
    print("Keyboard interrupt, exiting")
finally:
    sel.close()
