import socket
import re

# echo client on the same host and port


class Client:

    def __init__(self, name="unknown", port=12345, host="127.0.0.1"):
        self.name = name
        self.host = host
        self.port = port
        while True:
            try:
                self.name = input(
                    "Please enter your new Client name, no longer than 8 characters, \n at least 5 letters: ")
                if len(self.name) < 9 and re.match("[A-Za-z]{5}", self.name):
                    break
            except ValueError:
                print("Your name is not a string")
            except AttributeError:
                print("Your name is too long, enter one of max 8 characters.")
        print(f"This Client named {self.name} runs on a local host of 127.0.0.1")
        print("Please enter your PORT number for the Client, within range 1024 to 65535, be it same as Server")
        while True:
            try:
                self.port = int(input("Your PORT value:"))
                if 65536 > self.port > 1023:
                    print(f"The port is {self.port} and it is within allowed range")
                    break
                else:
                    print("Your value is not within allowed range")

            except ValueError:
                print("Your value is not a valid type(integer)")
        print(f"Your Client {self.name} has a HOST {self.host} and PORT {self.port}")

        # with as try, but used to close streams as well
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as my_cli_socket:
            my_cli_socket.connect((host, port))
            while True:
                print(f"Connected to server by {host} and {port}")
                print("Type your command : 'uptime', 'info', 'stop',\n or type 'help' for commands explanation")
                user_input = input("Type your command: ").encode("utf-8")
                my_cli_socket.send(user_input)
                recv_data = my_cli_socket.recv(1024).decode("utf-8")
                print(recv_data)

                if not recv_data:
                    print("Disconnected from server")
                    my_cli_socket.close()
                    break


