import datetime
import re
import json
import socket


class Server:

    def __init__(self, name="", port=int, host="127.0.0.1", start_time=None):
        self.name = name
        self.host = host
        self.port = port
        self.start_time = start_time
        # getting server name from user
        while True:
            try:
                self.name = input(
                    "Please enter your new server name, no longer than 8 characters, \n at least 5 letters: ")
                if len(self.name) < 9 and re.match("[A-Za-z]{5}", self.name):
                    break
            except ValueError:
                print("Your name is not a string")
            except AttributeError:
                print("Your name is too long, enter one of max 8 characters.")
        print(f"This server named {self.name} runs on a local host of 0.0.0.0")

        # getting server port from user
        print("Please enter your PORT number for the server, within range 1024 to 65535")
        while True:
            try:
                self.port = int(input("Your PORT value:"))
                if 65536 > self.port > 1023:
                    print(f"The port is {self.port} and it is within allowed range")
                    break
                else:
                    print("Your value is not within allowed range")
            except ValueError:
                print("Your value is not a valid type(integer)")
        print(f"Your server {self.name} has a HOST {self.host} and PORT {self.port}")

        # initialize start time of a Server instance
        self.start_time: datetime = datetime.datetime.now()

        server_info = "1.0.1-alpha"

        srv_commands: dict = {
            "help": "prints commands help",
            "info": "prints server version",
            "uptime": "prints server total runtime",
            "stop": "stop the server",
        }

        # providing server with a socket with created parameters
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as my_serv_socket:
            my_serv_socket.bind((self.host, self.port))
            my_serv_socket.listen()
            conn, addr = my_serv_socket.accept()
            with conn:

                while True:
                    data_in = conn.recv(1024)
                    cmd_data = data_in.decode("utf-8")

                    if not data_in:
                        print("Client disconnecting")
                        break
                    else:

                        match cmd_data:
                            case "info":
                                conn.send(bytes(json.dumps(f"Server version : {server_info},\n "
                                                           f"Server created : {self.start_time}"), encoding="utf-8"))
                            case "help":
                                conn.send(bytes(json.dumps(f"{srv_commands}"), encoding="utf-8"))
                            case "uptime":
                                serv_time = self.server_uptime(self.start_time)
                                conn.send(bytes(json.dumps(f"The server is running for {serv_time}"), encoding="utf-8"))
                            case "":
                                conn.send(bytes(json.dumps("Command unrecognized, type 'help' for information"),
                                                encoding="utf-8"))
                            case "stop":
                                break
                            case _:
                                conn.send(bytes(json.dumps("Command unrecognized, type 'help' for information"),
                                                encoding="utf-8"))

    # one method to return server running time
    def server_uptime(self, start_time):
        time_now = datetime.datetime.now()
        elapsed_time = time_now-start_time
        return elapsed_time


