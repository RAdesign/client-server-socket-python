from client_class import Client

print("Welcome to Server / Client Application, that will run both on your local machine.")
print("This Application will run a client")
try:
    newClient1 = Client()
except KeyboardInterrupt:
    print("Caught keyboard interrupt, exiting")
except TimeoutError:
    print("Ran out of time, exiting")