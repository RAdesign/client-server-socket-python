from server_class import Server


print("Welcome to Server / Client Application, that will run both on your local machine.")
print("This Application will run a server")
try:
    newServer1 = Server()
except KeyboardInterrupt:
    print("Caught keyboard interrupt, exiting")
except TimeoutError:
    print("Ran out of time, exiting")
finally:
    print("Client disconnected")
