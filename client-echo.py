# create a socket with socket() system call
# connect socket to the address of the server using connect() system call
# send and receive data, for example with read() and write() system calls
# close connection using close() system call

import socket

HOST = "127.0.0.1"   # echo client
PORT = 23456  # echo client

# with as try, but used to close streams as well
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as my_cli_socket:
    my_cli_socket.connect((HOST, PORT))
    my_cli_socket.sendall(b"Hi there")
    data = my_cli_socket.recv(2048)

print(f"Received {data!r}")

