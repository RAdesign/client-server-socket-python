import sys
import socket
import selectors
import types

# using selectors to keep up the connections of many
sel = selectors.DefaultSelector()


def accept_wrapper(sock):
    conn, addr = sock.accept()  # should be ready to read
    print(f"Accepted connection from {addr}")
    # want to unblock socket, because it will stall the entire server until returns
    conn.setblocking(False)
    # create an object to hold data you want included along with the socket with SimpleNamespace
    # bitwise OR to discern if client connection is ready for any action
    data = types.SimpleNamespace(addr=addr, inb=b"", outb=b"")
    events = selectors.EVENT_READ | selectors.EVENT_WRITE
    # events mask, socket, data objects are passed to sel.register()
    sel.register(conn, events, data=data)


# how the client connection is handled when it is ready
def service_connection(key, mask):
    sock = key.fileobj
    data = key.data
    # .key is named tuple returned from .select() - contains socket object(fileobj) and data object
    # .mask contains events that are ready
    # if socket is ready, mask and selectors. will evaluate True, calling sock.recv()
    if mask & selectors.EVENT_READ:
        recv_data = sock.recv(1024)  # should be ready to read
        if recv_data:
            # any data that is read is appended to data.outb, so that it can be sent later
            data.outb += recv_data
        else:
            # else block is checked if no data is received - means client closed socket
            print(f"Closing connection to {data.addr}")
            # need to unregister before closing, so it will be not monitored by .select()
            sel.unregister(sock)
            sock.close()
    if mask & selectors.EVENT_WRITE:
        # if socket is read for writing, any received data stored in data.outb is echoed to client with sock.send()
        if data.outb:
            print(f"Echoing {data.outb!r} to {data.addr}")
            sent = sock.send(data.outb)  # send method returns numbers of bytes sent
            data.outb = data.outb[sent:]  # data sent, removed from send buffer


# argv exception handling
if len(sys.argv) != 3:
    print(f"Usage: {sys.argv[0]} <host> <port>")
    sys.exit(1)

(host, port) = (sys.argv[1], int(sys.argv[2]))
lsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
lsock.bind((host, port))
lsock.listen()
print(f"Listening on {host, port}")
# call to setblocking to configure the socket in non-blocking mode. Calls to lsock will no longer block it
lsock.setblocking(False)
# registers socket to be monitored with sel.select() for events that you are interested in
# for listening socket you want to read events, data - that data will be returned as .select() returns
# use data to keep track of what it`s been sent and received on the socket
sel.register(lsock, selectors.EVENT_READ, data=None)


# event loop
try:
    while True:
        # blocks until there are sockets ready for I/O, returns list of tuples, each for one socket(key,mask)
        events = sel.select(timeout=None)
        # key is SelectorKey named tuple, contains fileobj attribute
        for key, mask in events:
            if key.data is None:
                # key.fileobj is socket object, mask is event mask of operations that are ready
                # if None - you need to accept connection, to get new socket object and register it with selector
                accept_wrapper(key.fileobj)
            else:
                # key.data is not none, socket is accepted, you need to service connection (w.mask argument)
                service_connection(key, mask)
except KeyboardInterrupt:
    print("Caught keyboard interrupt, exiting")
finally:
    sel.close()




