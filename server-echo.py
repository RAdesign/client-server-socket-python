# create a socket with a socket() system call
# bind the socket to the address using bind() system call, for a server socket on the
#   internet , ad address consists of a port number on the host machine
# listen for connections with a listen() system call
# accept connection with accept() system call. This usually blocks connection until a client connects to the server
# send and receive data using read() and write() system calls
# read close request from client and close connection with close()

import socket

HOST = "127.0.0.1"   # own ip address
PORT = 23456  # non privileged port for listening

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as my_serv_socket:
    my_serv_socket.bind((HOST, PORT))
    my_serv_socket.listen()
    conn, addr = my_serv_socket.accept()    # socket accepts connection form client, returns connection and tuple (host,port)-forIPv4- with client address
    with conn:                          # with is try except like
        print(f"Connected by {addr}")   # port address
        while True:
            data = conn.recv(2048)      # loop receives data as long as it comes
            if not data:
                break
            conn.sendall(data)          # loop echoes data back to the client
